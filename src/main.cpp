#include <Arduino.h>
#include "Wire.h"
#include "pthread.h"

#define MPU_ADDR 0x68
#define SDA_PIN 21
#define SCL_PIN 22

int16_t acc_x, acc_y, acc_z;
int16_t gyro_x, gyro_y, gyro_z;
int16_t temperature;

pthread_t tilt_switch_thread;

char tmp_str[7];

void *tilt_switch_task_entry(void* arg);

void convert_int16_to_str(char* buff, int16_t val){
  sprintf(buff, "%6d", val);
}


void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  pthread_create(&tilt_switch_thread, NULL, tilt_switch_task_entry, NULL);
  Wire.begin(SDA_PIN, SCL_PIN);
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
}

void loop() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_ADDR, 7*2, true);
  acc_x = (Wire.read() << 8) | Wire.read();
  acc_y = (Wire.read() << 8) | Wire.read();
  acc_z = (Wire.read() << 8) | Wire.read();
  temperature = (Wire.read() << 8) | Wire.read();
  gyro_x = (Wire.read() << 8) | Wire.read();
  gyro_y = (Wire.read() << 8) | Wire.read();
  gyro_z = (Wire.read() << 8) | Wire.read();

  char buffer[7];
  sprintf(buffer, "%6d", acc_x);
  Serial.print("aX = "); Serial.print(buffer);
  sprintf(buffer, "%6d", acc_y);
  Serial.print(" | aY = "); Serial.print(buffer);
  sprintf(buffer, "%6d", acc_z);
  Serial.print(" | aZ = "); Serial.println(buffer);

  // sprintf(buffer, "%6d", temperature/340.00 + 36.53);
  Serial.print("Temperature = "); Serial.println(temperature/340.00 + 36.53);

  sprintf(buffer, "%6d", gyro_x);
  Serial.print("gX = "); Serial.print(buffer);
  sprintf(buffer, "%6d", gyro_y);
  Serial.print(" | gY = "); Serial.print(buffer);
  sprintf(buffer, "%6d", gyro_z);
  Serial.print(" | gZ = "); Serial.println(buffer);

  Serial.println("------------------------------------------------");

  delay(1000);
  // put your main code here, to run repeatedly:
}

void *tilt_switch_task_entry(void* arg){
  const uint8_t tilt_switch_pin = 19;
  pinMode(tilt_switch_pin, INPUT);
  for(;;){
    if(digitalRead(tilt_switch_pin)){
      Serial.println("HIGH");
    } else {
      Serial.println("LOW");
    }
    delay(1000);
  }
}